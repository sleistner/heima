module Heima
  class BenchmarkResult
    attr_reader :avg, :requests

    def initialize(avg:, requests:)
      @avg = avg
      @requests = requests
    end
  end
end

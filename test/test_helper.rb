$LOAD_PATH.unshift File.expand_path("lib", __dir__)
require "heima"

require "minitest/autorun"

def capture(stream)
  # rubocop: disable Security/Eval, Style/EvalWithLocation
  begin
    stream = stream.to_s
    eval "$#{stream} = StringIO.new"
    yield
    result = eval("$#{stream}").string
  ensure
    eval("$#{stream} = #{stream.upcase}")
  end
  # rubocop: enable Security/Eval, Style/EvalWithLocation

  result&.split("\n")
end

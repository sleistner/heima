require "test_helper"

describe Heima::BenchmarkResult do
  def subject(*args)
    Heima::BenchmarkResult.new(*args)
  end

  describe "attr_reader" do
    it "provides avg" do
      assert_equal(2.1, subject(avg: 2.1, requests: [1, 2, 3]).avg)
    end
    it "provides requests" do
      assert_equal([1, 2, 3], subject(avg: 3, requests: [1, 2, 3]).requests)
    end
  end
end

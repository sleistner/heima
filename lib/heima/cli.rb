require "thor"

module Heima
  class CLI < Thor
    default_task :probe

    desc "probe uri [options]", "Messures average response time for the given uri"
    method_option :duration, type: :numeric, alias: "-d", desc: "The total duration in seconds to perform the probe."
    method_option :interval, type: :numeric, alias: "-i", desc: "The interval in seconds to perform the probe."
    def probe(uri)
      run_probe(Heima::Benchmark.new(uri: uri, **options))
    rescue URI::InvalidURIError
      error("Invalid URI")
    end

    no_commands do
      def run_probe(heima)
        say("Benchmarking #{heima.uri} (be patient)")
        Thread.new do
          1.upto(heima.duration) do
            say(".", nil, false)
            sleep(heima.interval)
          end
        end
        heima.run do |result|
          say("\nAverage response time: #{result.avg}ms")
        end
      end
    end

    desc "version", "Prints the version information"
    def version
      say(Heima::VERSION)
    end
    map %w[-v --version] => :version
  end
end

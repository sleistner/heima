require "test_helper"
require "thor"
require "thor/group"
require "stringio"

describe Heima::CLI do
  def subject(*args)
    Heima::CLI.new(*args)
  end

  before do
    Typhoeus.stub("example.com").and_return(Typhoeus::Response.new(code: 200, total_time: 2.2))
  end

  after do
    Typhoeus::Expectation.clear
  end

  describe "probe" do
    it "executes benchmark run and prints results" do
      cli = subject
      cli.options = { duration: 2, interval: 1 }
      lines = capture(:stdout) { cli.probe("example.com") }
      assert_equal("Benchmarking example.com (be patient)", lines.first)
      assert_match(/[\.]+/, lines[1])
      assert_equal("Average response time: 2200.0ms", lines.last)
    end

    it "rejects invalid uris" do
      lines = capture(:stderr) { subject.probe("example") }
      assert_equal("Invalid URI", lines.first)
    end
  end

  describe "version" do
    it "prints heima version number" do
      lines = capture(:stdout) { subject.version }
      assert_equal(Heima::VERSION, lines.first)
    end
  end
end
